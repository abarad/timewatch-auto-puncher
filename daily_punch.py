import timewatch


def punch_day(
    userid: int,
    company: int,
    password: str,
    punch_date: str,
    starttime: str,
    endtime: str,
    *args,
    **kwargs,
):
    tw = timewatch.TimeWatch(
        userid=userid,
        company=company,
        password=password,
    )
    tw.login()
    tw.punch(
        punch_date=punch_date,
        starttime=starttime,
        endtime=endtime,
    )
