import datetime
import time
import typing

import logger
import timewatch


def punch_month(
    userid: int,
    company: int,
    password: str,
    month_to_punch: int,
    starttime: str,
    endtime: str,
    working_days: typing.List[str],
    *args,
    **kwargs,
):
    _logger = logger.get_logger(
        name=__name__,
    )

    tw = timewatch.TimeWatch(
        userid=userid,
        company=company,
        password=password,
    )
    tw.login()

    first_day_of_month = datetime.date.today().replace(
        day=1,
    ).replace(
        month=month_to_punch,
    )

    next_month = first_day_of_month.replace(
        day=28,
    ) + datetime.timedelta(
        days=4,
    )

    last_day_of_month = next_month - datetime.timedelta(
        days=next_month.day,
    )

    date_to_punch = first_day_of_month

    while date_to_punch <= last_day_of_month:
        if date_to_punch.strftime('%A') in working_days:
            tw.punch(
                punch_date=date_to_punch.strftime('%Y-%m-%d'),
                starttime=starttime,
                endtime=endtime,
            )
            time.sleep(1)
        else:
            _logger.info(f'Skipping because {date_to_punch} is not a working day.')

        date_to_punch += datetime.timedelta(
            days=1,
        )

    _logger.info('Successfully punched whole month.')
